/**
 * @file
 * Quick_search_content angular app.
 */

'use strict';
var quick_search_content = angular.module('quick_search_content', []);

quick_search_content.controller('quickSearchContentController', ['$scope', '$http',
  function ($scope, $http) {
    $scope.isLoading = false;
    $http.get('/quick_search_content/services/node').
            success(function (data, status, headers, config) {
              $scope.nodes = data;
              $scope.isLoading = true;
            });
    $http.get('/quick_search_content/services/type').
            success(function (data, status, headers, config) {
              $scope.types = data;
            });

    $scope.sortBy = 'changed';
    $scope.orderByDesc = true;

    $scope.sortContent = function (field) {
      if ($scope.sortBy == field) {
        $scope.orderByDesc = !$scope.orderByDesc;
      }
      else {
        $scope.sortBy = field;
        $scope.orderByDesc = false;
      }
    };

    $scope.dateFormatFr = function (dateAformater) {
      $scope.dateFormater = new Date(dateAformater * 1000);
      return $scope.dateFormater;
    };
  }
]);

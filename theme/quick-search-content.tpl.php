<?php
/**
 * @file
 * Quick_search_content tpl.
 */
?>
<div ng-app="quick_search_content" ng-controller="quickSearchContentController">
    <div ng-if="!isLoading" style="text-align: center"><h3><?php print t('Loading') ?>...</h3></div>
    <div ng-if="isLoading">
        <div class="form-item form-type-textfield" style="float: left;margin: 0 15px 0 0 ">
            <label for="title"><?php print t('Title') ?></label>
            <input id="title" type="text" class="form-text" ng-model="query_title">
        </div>
        <div class="form-item form-type-select" style="float: left;margin: 0 15px 0 0 ">
            <label for="type"><?php print t('Content type') ?></label>
            <select id="type" class="form-select"  ng-model="query_type">
                <option value=""><?php print t('All') ?></option>
                <option ng-repeat="(key, data) in types" value="{{key}}">{{data}}</option>
            </select>
        </div>
        <div>
            <table>
                <tr>
                    <th>
                        <a href="#" ng-click="sortContent('title')"><?php print t('Title') ?>
                            <span ng-show="sortBy == 'title' && !orderByDesc"><b>&darr;</b></span>
                            <span ng-show="sortBy == 'title' && orderByDesc"><b>&uarr;</b></span>
                        </a>
                    </th>
                    <th>
                        <a href="#" ng-click="sortContent('type')"><?php print t('Content type') ?>
                            <span ng-show="sortBy == 'type' && !orderByDesc"><b>&darr;</b></span>
                            <span ng-show="sortBy == 'type' && orderByDesc"><b>&uarr;</b></span>
                        </a>
                    </th>
                    <th>
                        <a href="#" ng-click="sortContent('changed')"><?php print t('Updated') ?>
                            <span ng-show="sortBy == 'changed' && !orderByDesc"><b>&darr;</b></span>
                            <span ng-show="sortBy == 'changed' && orderByDesc"><b>&uarr;</b></span>
                        </a>
                    </th>
                    <th>Action</th>
                </tr>
                <tr ng-class-odd="'odd'" ng-class-even="'even'" ng-repeat="node in nodes| filter:{title: query_title, type: query_type} | orderBy:sortBy:orderByDesc">
                    <td>
                        <a href="/node/{{node.nid}}" target="_blank" title="Voir">{{node.title}}</a>
                    </td>
                    <td>
                        {{types[node.type]| uppercase}}
                    </td>
                    <td>
                        {{dateFormatFr(node.changed) | date:'yyyy/MM/dd - H:MM'}}
                    </td>
                    <td>
                        <a href="/node/{{node.nid}}/edit" target="_blank" title="Editer"><?php print t('Edit') ?></a>&nbsp;&nbsp;&nbsp;
                        <a href="/node/{{node.nid}}/delete" target="_blank" title="Supprimer"><?php print t('Delete') ?></a>&nbsp;&nbsp;&nbsp;
                    </td>
                </tr>
            </table>
        </div>
    </div>
</div>

CONTENTS OF THIS FILE
---------------------

 * DESCRIPTION
 * DEPENENCIES
 * INSTALLATION
 * MAINTAINERS


DESCRIPTION
-----------

  Provides a quick search engine for nodes

DEPENENCIES
-----------

  Libraries API : https://www.drupal.org/project/libraries

INSTALLATION
------------

  Download and install the dependencies
  Download angularJS : https://angularjs.org/
  and placed in sites/all/libraries/angular
  Go to admin/content/quick_search_content

MAINTAINERS
-----------

  David Witczak https://www.drupal.org/u/david-witczak
